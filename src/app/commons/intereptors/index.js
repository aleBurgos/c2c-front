import requestInterceptor from './request.interceptor.js';

export default   angular.module('app.commons.interceptors', ['app.commons.services'])
        .factory('RequestInterceptor',requestInterceptor)
        .config(['$httpProvider', function($httpProvider) {

            $httpProvider.interceptors.push('RequestInterceptor');
        }]);

