export default
    function requestInterceptor($q,$rootScope) {
        return {

            'request': function(config) {
                $rootScope.$broadcast('request::start',{});
                config.data = JSON.stringify(config.data);
                //config.headers.authorization = 'Bearer '+ authService.getToken();
                return config;
            },

            'response': function(response) {
                $rootScope.$broadcast('request::end',{});
                return response;
            },

            'responseError': function(rejection) {
                $rootScope.$broadcast('request::end',{});
                return $q.reject(rejection);
            },

            'requestError': function(rejection) {
                $rootScope.$broadcast('request::end',{});
                return $q.reject(rejection);
            }


        };
    };
