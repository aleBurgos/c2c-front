
export default function QuickSearch(){

        function QuickSearchController($parse){

        }


    return {
        restrict: 'E',
        templateUrl:'/app/commons/directives/quick-search/quick-search.html',
        transclude: false,
        controller:QuickSearchController,
        link: function(scope, element, attrs,$parse) {
            $(element).search();

            scope.$on('toggleSearchOverlay', function(scopeDetails, status) {

                if(status.show){
                    $(element).data('pg.search').toggleOverlay('show');
                } else {
                    $(element).data('pg.search').toggleOverlay('hide');
                }
            })
        }
    }
}