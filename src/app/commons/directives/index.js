import sidebar from './sidebar/sidebar';
import header from './header-menu/header'
import footer from './footer/footer'

export default
    angular.module('app.commons.directives', [ ])
        .directive('sidebar',sidebar)
        .directive('header',header)
        .directive('footer',footer);


