
export default function Footer(){
    return {
        restrict: 'E',
        templateUrl:'/app/commons/directives/footer/footer.html',
        transclude: false,
        link: function(scope, element, attrs) {
        }
    }}