
export default function Header(){
    return {
        restrict: 'E',
        templateUrl:'/app/commons/directives/header-menu/header-menu.html',
        transclude: false,
        link: function(scope, element, attrs) {
    }
}}