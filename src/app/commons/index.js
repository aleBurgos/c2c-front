
import directives from './directives/index';
import services from './services/index';
import interceptors from './intereptors/index';

 export default angular.module('app.commons', [
    directives.name,
    services.name,
    interceptors.name
  ]);
