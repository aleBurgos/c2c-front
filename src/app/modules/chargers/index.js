import angular from 'angular';
import routesConfig from './chargers.routes.js';
import Charger from './chargers.controller.js';
import './chargers.less';

export default angular
    .module('app.modules.chargers', [])
    .config(routesConfig)
    .component('charger',Charger);

