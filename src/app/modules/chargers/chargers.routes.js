export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider) {
    $stateProvider
        .state('chargers', {
            url: '/chargers' ,
            abstract: true,
            template: '<div ui-view></div>'
        })
        .state('chargers.list', {
            url: '',
            templateUrl: 'modules/chargers/chargers.html'

        })
}
