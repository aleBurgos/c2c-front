import angular from 'angular';
import 'angular-ui-router';
import chargers from './chargers/index';

export default angular
  .module('app.modules', [chargers.name]);
