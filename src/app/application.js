import  'jquery-sidebar';
import angular from 'angular';
import 'angular-ui-router';
//import 'angular-ui-utils';
import 'oclazyload';

import 'bootstrap';

import modules from './modules/index';
import commons from './commons/index';
import 'bootstrap/dist/css/bootstrap.css';
import './../styles.less';

angular
  .module('app', [ 'ui.router','oc.lazyLoad',modules.name,commons.name])
    .controller('AppCtrl', ['$scope', '$rootScope', '$state', function($scope, $rootScope, $state) {

        // App globals
        $scope.app = {
            name: 'CtwoC',
            description: 'Admin Dashboard UI kit',
            layout: {
                menuPin: false,
                menuBehind: false,
                theme: 'pages/css/pages.css'
            },
            author: 'Revox'
        };

        // Checks if the given state is the current state
        $scope.is = function(name) {
            return $state.is(name);
        };

        // Checks if the given state/child states are present
        $scope.includes = function(name) {
            return $state.includes(name);
        };

        // Broadcasts a message to pgSearch directive to toggle search overlay
        $scope.showSearchOverlay = function() {
            $scope.$broadcast('toggleSearchOverlay', {
                show: true
            })
        }

    }])
  .config(function( $urlRouterProvider, $locationProvider){
      $locationProvider.html5Mode(true).hashPrefix('!');
      $urlRouterProvider.otherwise('/');
    });

